<?php namespace PerfectNumberChallenge\Src;
require_once('src/Store.php');

class Number {
    const TYPE_PERFECT        = 'perfect';
    const TYPE_ABUNDANT       = 'abundant';
    const TYPE_DEFICIENT      = 'deficient';
    const TYPE_UNCLASSIFIABLE = 'unclassifiable';

    protected $classification;
    protected $divisors = [];

    /** 
     * @param \PerfectNumberChallenge\Src\Store
     */
    protected $store;

    /**
     * @param integer;
     */
    protected $val;

    /**
     * @param integer $int
     */
    public function __construct(int $int, Store $store)
    {
        $this->val   = $int;
        $this->store = $store;
    }

    /**
     * Getter for classification Property
     *
     * @return string
     */
    public function getClassification(): string
    {
        if (!is_null($this->classification)) {
            return $this->classification;
        }

        if ($this->isNegative() || $this->val === 0) {
            // Do not do negative numbers or 0
            $this->classification = self::TYPE_UNCLASSIFIABLE;
            return $this->classification;
        }

        if ($this->val === 1) {
            // 1 is deficient...
            $this->classification = self::TYPE_DEFICIENT;
            return $this->classification;
        }

        $this->classification = $this->getKnownClassifications();

        if (!is_null($this->classification)) {
            return $this->classification;
        }

        $this->classification = $this->tryAbundant();

        if (!is_null($this->classification)) {
            return $this->classification;
        }

        $this->generateDivisors();

        switch(array_sum($this->generateDivisors()) <=> $this->val) {
            case 0:
                $this->classification = self::TYPE_PERFECT;
                break;
            case 1:
                $this->classification = self::TYPE_ABUNDANT;
                break;
            case -1:
                $this->classification = self::TYPE_DEFICIENT;
                break;
        }

        $this->store->setValue($this->val, $this->classification);
        $this->store->write(json_encode($this->store->toArray()));

        return $this->classification;
    }

    /**
     * Is the number negative
     *
     * @return boolean
     */
    public function isNegative(): bool
    {
        return ($this->val < 0);
    }

    /**
     * Find divisors.
     *
     * @return array
     */
    protected function generateDivisors(): array
    {
        if (!empty($this->divisors)) {
            return $this->divisors;
        }

        if ($this->val === 1) {
            return [];
        }

        $divisors = [];

        for ($candidateDivisor = 1; $candidateDivisor < $this->val; $candidateDivisor++) {
            // Once we are past the square - we can break.
            if ($this->val / $candidateDivisor < $candidateDivisor) {
                break;
            }

            if ($this->val % $candidateDivisor === 0) {
                if ($this->val / $candidateDivisor === $candidateDivisor) {
                    // This is a square number
                    $divisors[] = $candidateDivisor;
                    break;
                } else if ($candidateDivisor === 1) {
                    $divisors[] = $candidateDivisor;
                } else {
                    $divisors[] = $candidateDivisor;
                    $divisors[] = $this->val / $candidateDivisor;
                }
            }
        }

        sort($divisors);

        $this->divisors = $divisors;

        return $this->divisors;
    }

    /**
     * Try a few shortcut rules
     *
     * @return string|null
     */
    protected function tryAbundant(): ?string
    {
        $tries = 100;

        // Every multiple of a perfect number is abundant
        foreach ($this->store->toArray()['perfect'] as $i => $candidateDivisor) {
            if ($this->val % $candidateDivisor === 0) {
                return self::TYPE_ABUNDANT;
            }

            if ($i >= $tries) {
                break;
            }
        }

        // Every multiple of an abundant number is abundant
        foreach ($this->store->toArray()['abundant'] as $i => $candidateDivisor) {
            if ($this->val % $candidateDivisor === 0) {
                return self::TYPE_ABUNDANT;
            }

            if ($i >= $tries) {
                break;
            }
        }

        return null;
    }

    /**
     * Try our store for matches
     *
     * @return string|null
     */
    protected function getKnownClassifications(): ?string
    {
        $knownClassifications = $this->store->toArray();

        foreach ($knownClassifications as $type => $numbers) {
            if (in_array($this->val, $numbers)) {
                return $type;
            }
        }

        return null;
    }

    /**
     * @return string;
     */
    public function __toString(): string
    {
        return (string) $this->val;
    }
}