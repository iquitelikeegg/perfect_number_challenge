<?php namespace PerfectNumberChallenge\Src;

class Store {
    private $filename;
    private $storepath;
    private $raw;
    private $array;

    const BASE_FILE = 'numbersbase.json';

    public function __construct(string $file)
    {
        $explodeFile = explode('/', $file);

        $this->storepath = implode('/', array_slice($explodeFile, 0, count($explodeFile) - 1));
        $this->filename = end($explodeFile);

        if (!file_exists($file)) {
            echo "\n\033[1;36mStorage file does not exist\n";
            echo "Creating file...\033[0m\n";
            copy($this->storepath . '/' . self::BASE_FILE, $file);
        }

        $this->raw = file_get_contents($file);
    }

    public function toArray(): array
    {
        if (!is_null($this->array)) {
            return $this->array;
        }

        $this->array = json_decode($this->raw, true);

        return $this->array;
    }

    public function write($jsonString)
    {
        $data = json_decode($jsonString, true);

        if (!array_key_exists('abundant', $data) || !array_key_exists('perfect', $data) || !array_key_exists('deficient', $data)) {
            throw new \Exception('Storage file data incorrectly formatted');
        }

        return file_put_contents($this->storepath . '/' . $this->filename, $jsonString);
    }

    public function setValue(int $number, string $key): void
    {
        array_push($this->array[$key], $number);
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getStorepath(): string
    {
        return $this->storepath;
    }
}