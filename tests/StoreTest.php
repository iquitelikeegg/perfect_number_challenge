<?php namespace PerfectNumberChallenge\Tests;

require_once('src/Store.php');

use PHPUnit\Framework\TestCase;
use PerfectNumberChallenge\Src\Store as Store;

class StoreTest extends TestCase {
    private $testFile = __DIR__ . '/../store/numberstest.json';

    public function testRetrieveStoreArray(): void
    {
        $store = new Store($this->testFile);

        $this->assertEquals(
            [
                'abundant' => [12,18,20],
                'perfect' => [6,28,496],
                'deficient' => [2,3,4]
            ],
            $store->toArray()
        );
    }

    public function testFilename(): void
    {
        $store = new Store($this->testFile);

        $this->assertEquals('numberstest.json', $store->getFilename());
    }

    public function testWrite(): void
    {
        $store = new Store($this->testFile);

        $this->assertFalse(
            false,
            $store->write('{"abundant" : [12,18,20],"perfect" : [6,28,496],"deficient" : [2,3,4]}')
        );
    }

    public function testWriteFail(): void
    {
        $store = new Store($this->testFile);

        $this->expectException(\Exception::class);

        $store->write('{"testInvalidData" : 12}');
    }

    public function testStorepath(): void
    {
        $store = new Store($this->testFile);

        $this->assertEquals(__DIR__ . '/../store', $store->getStorepath());
    }
}