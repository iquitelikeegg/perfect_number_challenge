<?php namespace PerfectNumberChallenge\Tests;

require_once('src/Number.php');
require_once('src/Store.php');

use PHPUnit\Framework\TestCase;
use PerfectNumberChallenge\Src\Number as Number;
use PerfectNumberChallenge\Src\Store as Store;

class NumberTest extends TestCase
{
    public function testClassification(): void
    {
        $store = new Store (__DIR__ . '/../store/numberstest.json');

        $perfect = new Number(6, $store);
        $this->assertEquals(Number::TYPE_PERFECT, $perfect->getClassification());

        $abundant = new Number(12, $store);
        $this->assertEquals(Number::TYPE_ABUNDANT, $abundant->getClassification());

        $deficient = new Number(5, $store);
        $this->assertEquals(Number::TYPE_DEFICIENT, $deficient->getClassification());
    }

    public function testIsNegative(): void
    {
        $negative = new Number(-1, self::getStore());

        $this->assertEquals(true, $negative->isNegative());
    }

    public function testFindDivisors(): void
    {
        $number = new Number(8, self::getStore());

        $findDivisors = self::getMethod('generateDivisors');

        // We know the divisors of this one!
        $this->assertEquals(
            [1,2,4],
            $findDivisors->invokeArgs($number, [])
        );
    }

    public function testToString(): void
    {
        $number = new Number(0, self::getStore());

        $this->assertEquals('0', $number);
    }

    public function testGetKnownClassifications(): void
    {
        $getKnownClassifications = self::getMethod('getKnownClassifications');

        $perfect = new Number(496, self::getStore());

        $this->assertEquals(
            Number::TYPE_PERFECT,
            $getKnownClassifications->invokeArgs($perfect, [])
        );        

        $abundant = new Number(18, self::getStore());

        $this->assertEquals(
            Number::TYPE_ABUNDANT,
            $getKnownClassifications->invokeArgs($abundant, [])
        );

        $deficient = new Number(5, self::getStore());

        $this->assertEquals(
            Number::TYPE_DEFICIENT,
            $getKnownClassifications->invokeArgs($deficient, [])
        );

        $unknown = new Number(-1234, self::getStore());

        $this->assertEquals(
            null,
            $getKnownClassifications->invokeArgs($unknown, [])
        );
    }

    public function testTryAbundant(): void
    {
        $tryAbundant = self::getMethod('tryAbundant');

        $abundantMultiple = new Number(36, self::getStore());

        $this->assertEquals(
            Number::TYPE_ABUNDANT,
            $tryAbundant->invokeArgs($abundantMultiple, [])
        );

        $abundantNonMultiple = new Number(-10, self::getStore());

        $this->assertEquals(
            null,
            $tryAbundant->invokeArgs($abundantNonMultiple, [])
        );
    }

    protected static function getStore()
    {
        return new Store(__DIR__ . '/../store/numberstest.json');
    }

    protected static function getMethod($name)
    {
        $number = new \ReflectionClass('\PerfectNumberChallenge\Src\Number');
        $method = $number->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }
}