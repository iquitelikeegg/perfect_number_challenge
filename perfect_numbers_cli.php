<?php 
$timeStart = microtime(true);
require_once('src/Number.php');
require_once('src/Store.php');

use PerfectNumberChallenge\Src\Number as Number;
use PerfectNumberChallenge\Src\Store as Store;

define('ROOT_PATH', __DIR__ . DIRECTORY_SEPARATOR);

function getClassification($testNumber): ?string
{
    if ($testNumber > PHP_INT_MAX) {
        return null;
    }

    $store  = new Store(ROOT_PATH . 'store/numbers.json');
    $number = new Number($testNumber, $store);

    return $number->getClassification();
}

if (count($argv) === 2 && $argv[1] === 'train') {
    echo "\n\n\033[1;36mtraining\033[0m\n";

    for ($i = 2; $i < 10000; $i++) {
        getClassification($i);
    }
} else if (!is_numeric($argv[1]) || count($argv) < 2 || strpos($argv[1], '.') !== false) {
    echo "\n\033[1;31mThe first argument passed to perfect_numbers_cli.php must be numeric\033[0m\n";
    echo "usage: \033[1;32mperfect_numbers_cli.php \033[1;33m<TEST NUMBER>\033[0m";
    echo " || \033[1;32mperfect_numbers_cli.php \033[1;33m<BASE> <CALCULATE FROM PRIME>\033[0m\n\n";
    exit;
} else {
    $candidateNumber = (count($argv) === 3) ? pow(2, $argv[1] - 1) * (pow(2, $argv[1]) - 1) : $argv[1];

    $numberClassification = getClassification($candidateNumber);

    switch ($numberClassification) {
        case "perfect" :
            echo "\n\033[1;32m$candidateNumber is PERFECT!\033[0m\n";
            break;
        case "abundant" :
            echo "\n\033[1;33m$candidateNumber is abundant\033[0m\n";
            break;
        case "deficient" :
            echo "\n\033[1;31m$candidateNumber is deficient...\033[0m\n";
            break;
        default:
            echo "\n\033[1;36m$candidateNumber certainly is a number.\033[0m\n";
            break;
    }
}

echo 'completed in ' . number_format((microtime(true) - $timeStart), 2) . " secs\n\n";