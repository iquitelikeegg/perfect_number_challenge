# Perfect Number Challenge (PHP) #

A number classification CLI script that takes a number and tells you whether or not the number is "perfect", "abundant" or "deficient".

### Usage ###

Before using:

```bash
php perfect_numbers_cli.php train
```

Then:

```bash
usage: php perfect_numbers_cli.php <TEST NUMBER> || php perfect_numbers_cli.php <BASE> <CALCULATE FROM PRIME?>
```

### Additional information ###

* This script will attempt to write (within the /store directory) in order to cache results for faster calculations, so it will need permissions to do so!

### Perfect, Abundant, and Deficient ###

Whether a number is one of these three categories is based on that number's [aliquot sum](https://en.wikipedia.org/wiki/Aliquot_sum). The aliquot sum is calculated by the sum of the divisors or factors of a number, not including the number itself.

For example, the proper divisors of 15 (that is, the positive divisors of 15 that are not equal to 15) are 1, 3 and 5, so the aliquot sum of 15 is 9 (1 + 3 + 5).

* **A perfect number** is where the aliquot sum = number
  * 6 is a perfect number because (1 + 2 + 3) = 6
* **An abundant number** is where the aliquot sum > number
  * 12 is an abundant number because (1 + 2 + 3 + 4 + 6) = 16
* **A deficient number** is where the aliquot sum < number
  * 8 is a deficient number because (1 + 2 + 4) = 7